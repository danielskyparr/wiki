[English](/Rules/NamingRules.md) | [Español](/Translated/Spanish/Rules/NamingRules.md) | [Romanian](/Translated/Romanian/Rules/NamingRules.md)

---
Please help us translate this page!
Contact us on discord for help on how to work with git, or submit a merge request if you know how!