[English](/Rules/Rules.md) | [Français](/Translated/French/Rules/rules.md) | [Español](/Translated/Spanish/Rules/rules.md)

---
# Reguli
Regulile din acest document se aplică pe discord cât și pe serverul de minecraft. Ele sunt un ghid, și până la urma urmei Adminii si Moderatorii au de luat decizia finală. Regulile nu sunt listate intr-o ordine anume pe langă Prima Regulă.

# Prima Regulă
Prima și cea mai importantă regulă este: foloseșteți creierul

Nu fi o pacoste pentru altii, și fii un om cu comportament decent. Nu ne fa să adăugăm o regula doar din cauza ta.

# Reguli globale
* Decizile echipei de moderație sunt finale. Dacă vrei să te plângi, contactează un admin, sau pe Jack dacă vrei să te plângi de un admin.
* Spamatul nu este permis. Depinde de canal cat de tare este respectată regula asta. Verifică [Utilizarea Canalelor](/Translated/Romanian/rules/ChannelUsage.md) pentru mai multe detalii.
* Poți să te certi despre aproape orice, dar atunci cand ajungeti la insulte personale atunci vom lua acțiune. Dacă vreți să va insultați faceți-o în DMuri.
* Rasismul și discriminarea de orice tip este ceva ce noi nu toleram.
* Nu avem pedepse pentru înjurături, dar foloseșteți creierul și nu înjura prea mult.
* Poți să iti promovezi **doar** proiectele legate de Code Lyoko dacă ai un motiv. Aceasta este la discretia moderatorilor. Dacă vrei să iti promovezi un alt proiect întreaba un admin înainte.
* **Avem restricții la ce nume poți folosi, poți afla mai multe informatii [aici](/Translated/Romanian/Rules/NamingRules.md)**
* Limba principala este Engleza, dar alte limbi pot fi vorbite in [canalele specifice](/Translated/Romanian/rules/ChannelUsage.md#Canale_pentru_alte_limbi)
* Conturile secundare nu sunt permise
* Content NSFW adică nuditatea și pornografia nu sunt permise. Glume și memeuri sunt permise în [canalele specifice](/Translated/Romanian/rules/ChannelUsage.md).
* Dacă încerci sa intrii din nou pe server după ce ai fost banat, vei fi banat din nou. Permanent. Pare destul de logic dar trebuie însă să scriu aceasta regulă.
* **Dacă ești un nesimtit in DMurile altor persoane sau pe alte servere poți fi banat aici**

# Reguli specifice pe discord
* Dacă tot mentionezi pe cineva în continuu, vei primi mute, sau chiar mai rau.
* Sa trollezi/enervezi persoane cu boții nu este permis. Dacă cineva se plange, mereu tu vei fi cel care a greșit.

# Reguli specifice pe minecraft

- Dacă trollezi utilizand mechanici ale jocului nu este permis. Exemplele includ dar nu sunt limitate la: 
    * Schimbarea destinatiei scanerului fara a întreba
    * Spamatul întoarcerei in timp
    * Pvp cu alți playeri în contiinu chiar dacă nu au fost xanafied